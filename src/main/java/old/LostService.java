package old;

import app.model.Lost;
import app.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
@Transactional
@Service
public class LostService {
    @Autowired
    private ImageRepository imageRepository;
    @PersistenceContext
    private EntityManager entityManager;
    public  LostService(ImageRepository imageRepository){
        this.imageRepository=imageRepository;
    }

    public String SaveItem( Lost lost,MultipartFile file) throws SQLException {
        try {
            Blob blob = new SerialBlob(file.getBytes());
            lost.setImage(blob);
            entityManager.persist(lost);;
            return "saved";
        } catch (IOException e) {
            e.printStackTrace();
            return "not saved";
        }


    }}


