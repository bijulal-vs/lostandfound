package old;

import app.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Service
@Transactional
public class Forgotpassword{
    @PersistenceContext
    private EntityManager entityManager;
    private final UserRepository userRepository;
    public Forgotpassword(UserRepository userRepository)
    {this.userRepository=userRepository;}


    public int ChangePassword(String email, String password){
      try{

         Query query=entityManager.createQuery("UPDATE userclass set password= :password " +
                 "where email= :email");
           return  query.setParameter("password",password).setParameter("email",email).executeUpdate();
      }catch (Exception e)
      {e.printStackTrace();
      return 0;}


    }



}

