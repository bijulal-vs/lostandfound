package app.model;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.sql.Blob;
import java.sql.Date;

@Entity
@Table(name="lost_items")
public class Lost{
    @Id
    private int Itemid;
    @Lob
    private Blob Image;
    private String Description;
    private String Itemname;
    private long Phone;
    @Column(name="LostDate")
    private Date LostDate;
    @Column(name="LostPlace")
    private String LostPlace;
    public Lost() {
    }

    public Lost(int itemid, String description, String itemname, long phone, Date lostDate, String lostPlace) {
        Itemid = itemid;
        Description = description;
        Itemname = itemname;
        Phone = phone;
        LostDate = lostDate;
        LostPlace = lostPlace;
    }

    public int getItemid() {
        return Itemid;
    }

    public Blob getImage() { return Image; }

    public String getDescription() {
        return Description;
    }
    @DecimalMin(value = "1000000000",message = "mobile number must be 10 digits")
    @DecimalMax(value = "9999999999",message = "mobile number must be 10 digits")
    public long getPhone() {
        return Phone;
    }

    public Date getLostDate() { return LostDate; }

    public String getLostPlace() {return LostPlace;}

    public void setItemid(int itemid) {
        Itemid = itemid;
    }

    public void setImage(Blob image) {
        Image = image;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setPhone(long phone) {
        this.Phone = phone;
    }

    public void setLostDate(Date lostDate) { LostDate = lostDate;  }

    public void setLostPlace(String lostPlace) { LostPlace = lostPlace;}
    @NotBlank(message = "Item name cannot be empty")
    public String getItemname() {
        return Itemname;
    }

    public void setItemname(String itemname) {
        Itemname = itemname;
    }
}

