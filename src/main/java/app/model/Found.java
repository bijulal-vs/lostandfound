package app.model;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Blob;
import java.sql.Date;

@Entity
@Table(name="found_items")
public class Found{
    @Id
    @Column(name="idfound_items")
    private int id;
    @Column(name="item_name")
    private String itemname;
    @Lob
    @Column(name="Image")
    private Blob Image;
    @Column(name="item_description")
    private String Description;
    @Column(name="Phone")
    private long Phone;
    @Column(name="found_date")
    private Date FoundDate;
    @Column(name="found_place")
    private String FoundPlace;
    @NotBlank(message = "Item name cannot be blank")
    public String getItemname() {
        return itemname;
    }

    public Blob getImage() {
        return Image;
    }

    public String getDescription() {
        return Description;
    }
    @DecimalMax(value = "9999999999",message = "mobile number should be 10 digits")
    @DecimalMin(value = "1000000000",message = "mobile number should be 10 digits")
    public long getPhone() {
        return Phone;
    }

    public int getId() {
        return id;
    }
    public Date getFoundDate() {
        return FoundDate;
    }

    public String getFoundPlace() {
        return FoundPlace;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public void setImage(Blob image) {
        Image = image;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setPhone(long phone) {
        Phone = phone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFoundDate(Date foundDate) {
        FoundDate = foundDate;
    }

    public void setFoundPlace(String foundPlace) {
        FoundPlace = foundPlace;
    }

    public Found() {
    }

    public Found(int id, String itemname, String description, long phone, Date foundDate, String foundPlace) {
        this.id = id;
        this.itemname = itemname;
        Description = description;
        Phone = phone;
        FoundDate = foundDate;
        FoundPlace = foundPlace;
    }

    @Override
    public String toString() {
        return "Found{" +
                "id=" + id +
                ", itemname='" + itemname + '\'' +
                ", Description='" + Description + '\'' +
                ", Phone=" + Phone +
                ", FoundDate=" + FoundDate +
                ", FoundPlace='" + FoundPlace + '\'' +
                '}';
    }
}
