package app.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int role_id;
    private String role_name;

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public Role(Role role) {
        this.role_id=role.getRole_id();
        this.role_name=role.getRole_name();
    }

    public Role(String role_name) {
        this.role_name = role_name;
    }

    public Role(){}

    @Override
    public String toString() {
        return  role_name ;
    }

    @Override
    public String getAuthority() {
        return "ROLE_"+this.role_name;
    }
}
