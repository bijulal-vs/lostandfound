package app.model;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Set;


@Entity
@Table(name = "users")
public class userclass {


    private String firstname;
    private String  lastname;
    private String email;
    private String address;
    private int age;
    private int user_id;
    @Id
    private long phone;
    private String password;
    @OneToMany(cascade = CascadeType.ALL,fetch =FetchType.EAGER )
    @JoinTable(name = "users_role",joinColumns =@JoinColumn(name = "user_id"),inverseJoinColumns=@JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public userclass(userclass user) {
        this.firstname=user.firstname;
        this.lastname=user.getLastname();
        this.email=user.getEmail();
        this.address=user.getAddress();
        this.age=user.getAge();
        this.user_id=user.getUser_id();
        this.phone=user.getPhone();
        this.password=user.getPassword();
        this.roles=user.getRoles();
    }


    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @NotBlank(message="firstname is mandatory")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    @NotBlank(message="lastname is mandatory")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    @NotBlank(message="email cant be empty")
    @Email(message = "please enter a valid email address")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @NotBlank
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    @DecimalMax(value ="9999999999",message = "phone number should be 10 digits")
    @DecimalMin(value="1000000000", message = "phone number should be 10 digits")
    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }
    @NotBlank(message = "password can't be empty")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password =password;
    }

    public userclass() {
    }

    public userclass(String firstname, String lastname, String email, String address, int age, int user_id, long phone, String password, Set<Role> roles) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.age = age;
        this.user_id = user_id;
        this.phone = phone;
        this.password = password;
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "userclass{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                ", user_id=" + user_id +
                ", phone=" + phone +
                ", password='" + password + '\'' +
                ",Roles="+roles+
                '}';
    }
}
