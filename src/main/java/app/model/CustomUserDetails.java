package app.model;



import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@ComponentScan
public class CustomUserDetails  implements UserDetails {
        public  userclass user;
        private String token;
        private String[] Role;
    public CustomUserDetails(userclass user) {
        this.user=user;
    }
    public CustomUserDetails() {
         }


    public Set<Role> GetRole(){
        return this.user.getRoles();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      return   user.getRoles();
    }

    @Override
    public String getPassword() {
       return user.getPassword();
    }

    @Override
    public String getUsername() {
       return user.getEmail();
    }

    public void setUser(userclass user) {
        this.user = user;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String[] getRole() {
        return Role;
    }

    public void setRole(String[] role) {
        Role = role;
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "user=" + user +
                ", token='" + token + '\'' +
                ", Role=" + Arrays.toString(Role) +
                '}';
    }
}
