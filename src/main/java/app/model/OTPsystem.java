package app.model;

public class OTPsystem {
private String otp;
private  long expriy;

    public OTPsystem() { }

    public String getOtp() {
        return otp;
    }

    public long getExpriy() {
        return expriy;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setExpriy(long expriy) { this.expriy = expriy; }

    public OTPsystem GenerateOTP(){

        this.setOtp(String.valueOf(((int)(Math.random()*(1000000-100000)))+100000));
        this.setExpriy(System.currentTimeMillis()+300000);
        return this;
    }
}
