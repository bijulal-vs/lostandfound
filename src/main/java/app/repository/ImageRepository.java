package app.repository;

import app.model.Lost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Lost,Integer> {
}
