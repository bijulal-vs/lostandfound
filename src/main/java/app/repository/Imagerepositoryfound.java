package app.repository;

import app.model.Found;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Imagerepositoryfound extends JpaRepository<Found,Integer> {
}
