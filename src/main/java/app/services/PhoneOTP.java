package app.services;

import org.springframework.http.ResponseEntity;

public interface PhoneOTP {
     ResponseEntity SendOTP(String mobilenumber);
     ResponseEntity VerifyOTP(String mobilenumber,String otp);
}
