package app.services;

import app.model.userclass;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface SaveUser {
     ResponseEntity RegisterUser(userclass user,BindingResult bindingResult);
}
