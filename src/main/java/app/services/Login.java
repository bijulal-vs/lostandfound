package app.services;

import org.springframework.http.ResponseEntity;
import java.util.Map;

public interface Login {
    ResponseEntity UserLogin(long phone, String email,String password);
    Map<String ,String > ShowAllUsers();
}
