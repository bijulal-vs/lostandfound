package app.services;

import app.model.Lost;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.sql.SQLException;
import java.util.List;

public interface Lostitem {
    String SaveLostItem(Lost lost,MultipartFile file,BindingResult bindingResult) throws SQLException;
    List<Lost> Showlostitems();
    List<Lost> Searchitems(String itemname);
    String DeleteLostItem(int id, long phone);
}
