package app.services;

import app.model.userclass;
import app.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import app.repository.UserRepository;
import org.springframework.validation.BindingResult;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
@Transactional
public class UserService {
    @Autowired
    private final UserRepository userRepository;
    @Autowired
    SecurityConfig security;

    @PersistenceContext
    private EntityManager entityManager;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void Saveuser(userclass user, BindingResult bindingResult) {
        user.setPassword(security.passwordEncoder().encode(user.getPassword()));
        System.out.println(user.getPassword());
        entityManager.persist(user); }

    public boolean SearchByphone(long phone) {
        return userRepository.existsById(phone);
    }

    public userclass getbyphone(long phone) {
        return entityManager.find(userclass.class, phone);
    }

    public userclass getbyemail(String email) {
        try
        {
            Query query= entityManager.createQuery("from userclass where email= :email");
            return (userclass)query.setParameter("email", email).getSingleResult();
        } catch (NoResultException e)
        {
            return null;
        }
    }

    public int ChangePassword(String email, String password){
        try
        {
            Query query=entityManager.createQuery("UPDATE userclass set password= :password " +
                    "where email= :email");
            return  query.setParameter("password",security.passwordEncoder().encode(password)).setParameter("email",email).executeUpdate();
        } catch (Exception e)
         {
            e.printStackTrace();
            return 0;
         } }

    public List<userclass> getallusers()
    {
        try
        {Query query=entityManager.createQuery("from userclass order by FirstName");
        return (List<userclass>)query.getResultList();}
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public String GetAddress(long Phone)
    {
        try{
            Query query=entityManager.createQuery("from userclass u where u.phone='" +Phone+"'");
            userclass user=(userclass) query.getSingleResult();
            return "email: "+user.getEmail()+"  Address :"+user.getAddress();
        }catch(Exception e){
            e.printStackTrace();
            return "cannot find user";
        }
    }

    public Map<String,String> getallusernames()
    {   Map<String,String> usermap=new HashMap<>();
        List<userclass> userlist=getallusers();
        for(userclass user:userlist)
            usermap.put(user.getFirstname(),user.getLastname());
        return usermap;
    }

    public userclass getbyemailid(String email) {
        try
        {
            Query query= entityManager.createQuery("from userclass where email= :email");
            return (userclass) query.setParameter("email", email).getSingleResult();

        } catch (NoResultException e)
        {
            return null;
        }

    }
}