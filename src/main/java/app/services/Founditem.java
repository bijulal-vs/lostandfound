package app.services;

import app.model.Found;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface Founditem {
String AddFoundItem(Found found,MultipartFile file,BindingResult bindingResult) throws IOException;
List<Found> Searchitem(String itemname);
List<Found> Showfounditems();
String DeleteFoundItem(int id, long phone);
}

