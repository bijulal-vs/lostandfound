package app.services;

import org.springframework.http.ResponseEntity;

public interface MailOTP {
    ResponseEntity SendOTP(String email);
    ResponseEntity VerifyOTP(String email,String otp);

}
