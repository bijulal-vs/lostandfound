package app.services;

import app.model.Found;
import app.model.Lost;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface AdminServices {
    List<Lost> GetLostItems();
    List<Found> GetFoundItems();
    String RemoveLostItem(int id);
    String RemoveFoundItem(int id);
    ResponseEntity AdminLogin(String Username, String Password);
    String EditLostItems(int id, Lost lost, MultipartFile file) throws IOException;

    String EditFoundItems(int id, Found found, MultipartFile file) throws IOException;
}
