package app.services;

import org.springframework.http.ResponseEntity;

public interface ForgotPassword {
     ResponseEntity GetOTP(String email);
     ResponseEntity ChangePassword(String email, String OTP, String newpassword);
}
