package app.security;

import app.model.CustomUserDetails;
import app.model.Role;
import app.model.userclass;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class JwtValidator {
    public CustomUserDetails valdate(String token) {
        CustomUserDetails user=null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey("LostAndFound")
                    .parseClaimsJws(token)
                    .getBody();

             user=new CustomUserDetails(new userclass());
            user.setToken(token);
            user.user.setEmail(body.getSubject());
            user.user.setUser_id(Integer.parseInt((String) body.get("userId")));
            user.user.setPhone(Long.parseLong((String) body.get("phone")));
            user.user.setFirstname((String)body.get("firstname"));
            user.user.setLastname((String)body.get("lastname"));
            user.user.setAddress((String)body.get("address"));
            user.user.setAge(Integer.parseInt((String)body.get("age")));
            String[] roles = body.get("role").toString().split(",");
            user.setRole(roles);

        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return user;

    }
}
