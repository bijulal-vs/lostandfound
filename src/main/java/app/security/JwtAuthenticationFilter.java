package app.security;

import app.model.JwtAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Component
public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter{

    private JwtSuccesshandler authenticationSuccesshandler;
    JwtAuthenticationToken token;

    protected JwtAuthenticationFilter() {
        super("/admin/**");
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String header=httpServletRequest.getHeader("Authorization");
        if(header==null||header.startsWith("Token "))
            throw new RuntimeException("JWT token missing");
        String Authtoken=header.substring(7);
        token=new JwtAuthenticationToken(Authtoken);
        return  getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
     super.successfulAuthentication(request, response, chain, authResult);
//        response.addHeader("Authorization",token.getToken());
     chain.doFilter(request,response);

    }

    public void setAuthenticationSuccesshandler(JwtSuccesshandler authenticationSuccesshandler) {
       this.authenticationSuccesshandler = authenticationSuccesshandler;
    }


}
