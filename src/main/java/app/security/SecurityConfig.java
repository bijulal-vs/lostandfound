package app.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Collections;


@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

@Autowired
 private CustomAuthProvider authProvider;

        @Autowired
        private CustomUserDetailsService customUserDetailsService;
    private JWTAuthenticantionEntryPoint entrypoint;

        @Bean
        public AuthenticationManager authenticationManager(){
            return new ProviderManager(Collections.singletonList(authProvider));
        }

        @Bean
        public JwtAuthenticationFilter jwtAuthenticationFilter(){

            JwtAuthenticationFilter filter=new JwtAuthenticationFilter();
            filter.setAuthenticationManager(authenticationManager());
            filter.setAuthenticationSuccesshandler(new JwtSuccesshandler());
            return filter;
        }


        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(customUserDetailsService)
                    .passwordEncoder(passwordEncoder())
                    .and()
                    .authenticationProvider(authProvider);
        }

        @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
        }

        @Override
    protected void configure(HttpSecurity http) throws Exception {

//
//                http    .httpBasic()
//                        .and()
//                        .authorizeRequests()
//                        .antMatchers("/register","/welcome","/admin/login").permitAll()
//                        .antMatchers("/admin/**").hasRole("ADMIN")
//                        .anyRequest().authenticated().and()
//                        .formLogin().permitAll();
//                http.csrf().disable();


            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/register","/welcome","/admin/login","/login").permitAll()
                    .and()
                    .authorizeRequests().anyRequest().authenticated()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(entrypoint)
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .addFilterBefore(jwtAuthenticationFilter(),UsernamePasswordAuthenticationFilter.class);
            http.headers().cacheControl();




        }

}


