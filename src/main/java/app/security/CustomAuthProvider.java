package app.security;

import app.model.CustomUserDetails;
import app.model.JwtAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class CustomAuthProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    JwtValidator validator;
    @Autowired
   private CustomUserDetailsService userservice;
    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }
    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        JwtAuthenticationToken jwttoken=(JwtAuthenticationToken)usernamePasswordAuthenticationToken;
        String token=jwttoken.getToken();
        CustomUserDetails user =validator.valdate(token);
        if(user==null)
            throw new RuntimeException("JWT token not found");

        System.out.println("retrive usere------"+user.toString());
            return  user;


    }

    @Override
    public boolean supports(Class<?> authentication) {
//        System.out.println( "supports");return super.supports(authentication);
//        return authentication.equals( UsernamePasswordAuthenticationToken.class );
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));

    }
}
