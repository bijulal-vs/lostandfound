package app.security;

import app.model.CustomUserDetails;
import app.model.userclass;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        userclass user = userService.getbyemailid(email);
        if (user == null)
            throw new UsernameNotFoundException("no user with email " + email);

        CustomUserDetails customuser = new CustomUserDetails(user);
        return customuser;
    }
}

//pASS JWT TOKEN ALSO, MOVE from loginimpl