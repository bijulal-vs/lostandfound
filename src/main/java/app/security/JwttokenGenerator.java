package app.security;

import app.model.CustomUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

@Component
public class JwttokenGenerator {
    public String Generate(CustomUserDetails user){
        Claims claims= Jwts.claims()
                .setSubject(user.getUsername());
        claims.put("userId",String .valueOf(user.user.getUser_id()));
        claims.put("role",user.user.getRoles());
        claims.put("lastname",user.user.getLastname());
        claims.put("firstname",user.user.getFirstname());
        claims.put("phone",String.valueOf(user.user.getPhone()));
        claims.put("age",String.valueOf(user.user.getAge()));
        claims.put("address",user.user.getAddress());


        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512,"LostAndFound")
                .compact();
    }}
