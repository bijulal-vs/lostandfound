package app.controllers;

import app.services.PhoneOTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PhoneOTPcontroller {

    @Autowired
    private PhoneOTP phoneotp;


    @RequestMapping(value = "/sendotp/{mobilenumber}",method = RequestMethod.POST)
    public ResponseEntity sendotp(@PathVariable ("mobilenumber") String mobilenumber){
        try {
            return  phoneotp.SendOTP(mobilenumber);
        }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<>("Cannot send OTP,Please try again",HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/verifyotp/{mobilenumber}/{otp}",method = RequestMethod.POST)
    public ResponseEntity verifyOTP(@PathVariable("mobilenumber") String mobilenumber,@PathVariable("otp") String otp){
        try{
            return phoneotp.VerifyOTP(mobilenumber,otp);
        }catch(Exception e)
        {
          e.printStackTrace();
          return new ResponseEntity<>("otp not verified, Please try again",HttpStatus.BAD_REQUEST);
        }
    }
}


