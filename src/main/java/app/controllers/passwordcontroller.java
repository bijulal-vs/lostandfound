package app.controllers;

import app.services.ForgotPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class passwordcontroller {

    @Autowired
    private ForgotPassword forgotPassword;


    @ResponseBody
    @GetMapping("/forgotpassword/{email}")
    public ResponseEntity ChangePasswordOTP(@PathVariable("email") String email) {
        try {
            return forgotPassword.GetOTP(email);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("cannot send OTP!please try again.", HttpStatus.BAD_GATEWAY);
        }
    }

    @PostMapping("forgotpassword/{email}/{otp}/{password}")
    public ResponseEntity ChangePassword(@PathVariable("email") String email, @PathVariable("otp") String Otp, @PathVariable("password") String password) {
        try {
            return forgotPassword.ChangePassword(email, Otp, password);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("password not changed, please try again.", HttpStatus.BAD_GATEWAY);

        }
    }
}








