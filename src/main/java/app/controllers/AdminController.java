package app.controllers;

import app.model.Found;
import app.model.Lost;
import app.services.AdminServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(value = "/admin")
//@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    @Autowired
    private AdminServices adminServices;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResponseEntity Adminlogin(@RequestParam("username") String username,@RequestParam("password") String password){
        try{
            return adminServices.AdminLogin(username, password);
        }catch(Exception e)
        {
            e.printStackTrace();
            return new ResponseEntity<>("cannot login, please try again",HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/showlostitems",method = RequestMethod.GET)
    public List<Lost> Adminshowlostitem(){
        try{
            return adminServices.GetLostItems();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/showfounditems",method = RequestMethod.GET)
    public List<Found> Adminshowfounditem(){
        try{
            return adminServices.GetFoundItems();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/removefounditems/{id}",method = RequestMethod.POST)
    public String Adminremovefounditem(@PathVariable("id") int id){
        try{
            return adminServices.RemoveFoundItem(id);
        }catch (Exception e){
            e.printStackTrace();
            return "please try again";
        }
    }
    @RequestMapping(value = "/removelostitems/{id}",method = RequestMethod.POST)
    public String Adminremovelostitem(@PathVariable("id") int id){
        try{
            return adminServices.RemoveLostItem(id);
        }catch (Exception e){
            e.printStackTrace();
            return "please try again";
        }
    }
    @RequestMapping(value = "/editlostitem",method = RequestMethod.POST)
    public String EditLostItem(@RequestParam("itemid") int id, @ModelAttribute("lostitem") Lost lost, @RequestParam("file")MultipartFile file)
    {
        try
        {
            return adminServices.EditLostItems( id,lost,file);
        }catch(Exception e){
            e.printStackTrace();
            return "not edited";
        }

    }

    @RequestMapping(value = "/editfounditem",method = RequestMethod.POST)
    public String EditFoundItem(@RequestParam("itemid") int id, @ModelAttribute("founditem") Found found, @RequestParam("file")MultipartFile file)
    {
        try
        {
            return adminServices.EditFoundItems( id,found,file);
        }catch(Exception e){
            e.printStackTrace();
            return "not edited";
        }

    }
}
