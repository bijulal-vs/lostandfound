package app.controllers;

import app.model.Found;
import app.services.Founditem;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;


@RestController
//@PreAuthorize("hasAnyRole('ADMIN','USER')")
@RequestMapping(value = "/found")
public class FoundController {
    @Autowired
    private Founditem founditem;
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/savefounditem", method = RequestMethod.POST, headers = ("content-type=multipart/*"))
    public String SaveFoundItem(@Valid @ModelAttribute("founditem")Found found, @RequestParam("file") MultipartFile file, BindingResult bindingResult)  {
        try {
            return founditem.AddFoundItem(found,file,bindingResult);
        } catch (Exception E) {
            E.printStackTrace();
            return "item not saved";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/searchfounditems",method = RequestMethod.POST)
    public List<Found> Searchfounditems(@RequestParam("itemname") String itemname){
        try{

            return founditem.Searchitem(itemname);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/showfounditems",method =RequestMethod.POST)
    public List<Found> Showfounditems(){
        try
    {
        return founditem.Showfounditems();
    }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/getuser")
    public String Getuser(@RequestParam("phone") long phone){
        return userService.GetAddress(phone);
    }

    @ResponseBody
    @RequestMapping(value = "/deletefounditem",method = RequestMethod.POST)
    public String Deletefound(@RequestParam("itemId") int id,@RequestParam("phone")long phone){
        try{
            return founditem.DeleteFoundItem(id,phone);
        }catch(Exception e){
            e.printStackTrace();
            return "an internal error occoured, try again";
        }
    }

}
