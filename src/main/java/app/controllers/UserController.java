package app.controllers;

import app.model.userclass;
import app.security.CustomAuthProvider;
import app.services.Login;
import app.services.SaveUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;


@RestController
@RequestMapping()
public class UserController {
    @Autowired
    private SaveUser saveUser;
    @Autowired
    private Login login;
    @Autowired
    private CustomAuthProvider provider;


    @ResponseBody
    @PostMapping(value = "/register")
    public ResponseEntity RegisterUser(@Valid  @RequestBody userclass user,BindingResult bindingResult) {
        try{
            return saveUser.RegisterUser(user,bindingResult);
        }catch (Exception e)
        {e.printStackTrace();
         return new ResponseEntity<>("cannot save user, please try again",HttpStatus.BAD_REQUEST);}
    }

    @ResponseBody
    @GetMapping(value = "/login")
    public ResponseEntity Userlogin(@RequestParam(value = "phone", required = false, defaultValue = "0") Long phone, @RequestParam(value = "email", required = false, defaultValue = "default") String email,  @RequestParam String password) {
        try{
            return login.UserLogin(phone,email,password);
            }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<>("login failed, please try again",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/showallusers")
    public Map<String, String> ShowAllUsers(){
        try{
            return login.ShowAllUsers();
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        }
//
//        @GetMapping(value = "/test")
//    public String testcontroller(@RequestHeader("Authorization") String token){
//        provider.
//        }

}


