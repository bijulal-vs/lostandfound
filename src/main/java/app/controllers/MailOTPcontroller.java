package app.controllers;

import app.services.MailOTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


@RestController
public class MailOTPcontroller{
    @Autowired
    private MailOTP mailOTP;

    @RequestMapping(value = "/emailgetotp/{email}", method = RequestMethod.POST)
    public ResponseEntity SendOtpMail(@PathVariable("email") String email)
    {
        try{
             return mailOTP.SendOTP(email);
            }catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("cannot send OTP! Please try again.",HttpStatus.BAD_GATEWAY);
        }
    }


    @RequestMapping(value = "/emailverifyotp/{email}/{otp}",method = RequestMethod.POST)
    public ResponseEntity VerifyOtpMail(@Valid @PathVariable("email") @Email String email, @PathVariable("otp") @NotBlank String otp)
    {
        try{
            return mailOTP.VerifyOTP(email, otp);
            }catch(Exception e){
                e.printStackTrace();
                return new ResponseEntity<>("OTP not verified! Please try again.",HttpStatus.BAD_GATEWAY);

        }
    }


    }









