package app.controllers;

import app.model.Lost;
import app.services.Lostitem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@PreAuthorize("hasAnyRole('ADMIN','USER')")
public class LostController {
    @Autowired
    Lostitem lostitem;

    @RequestMapping(value = "/savelostitem", method = RequestMethod.POST, headers = ("content-type=multipart/*"))
    public String SaveLostItem(@Valid @ModelAttribute("lostitem") Lost lost, @RequestParam("file") MultipartFile file, BindingResult bindingResult) {
        try {
            return lostitem.SaveLostItem(lost, file,bindingResult);
        } catch (Exception E) {
            E.printStackTrace();
            return "item not saved";
        }
    }

    @RequestMapping(value = "/showlostitems",method =RequestMethod.POST)
    public List<Lost> Showfounditems(){
        try
        {
            return lostitem.Showlostitems();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/searchlostitems",method = RequestMethod.POST)
    public List<Lost> Searchfounditems(@RequestParam("itemname") String itemname){
        try{

            return lostitem.Searchitems(itemname);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

       }

    @ResponseBody
    @RequestMapping(value = "/deletelostitem",method = RequestMethod.POST)
    public String Deletefound(@RequestParam("itemId") int id,@RequestParam("phone")long phone){
        try{
            return lostitem.DeleteLostItem(id, phone);
        }catch(Exception e){
            e.printStackTrace();
            return "an internal error occoured, try again";
        }

    }


}