package app.ServiceImpl;

import app.model.userclass;
import app.services.SaveUser;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

@Service
@Transactional
public class SaveUserimpl implements SaveUser {

    @Autowired
    private UserService userService;


    @Override
    public ResponseEntity RegisterUser(userclass user,BindingResult bindingResult) {
        if(userService.getbyemail(user.getEmail())==null&&!(userService.SearchByphone(user.getPhone()))){
            if(bindingResult.hasErrors())
                return new ResponseEntity<>("please provide valid user details /n"+bindingResult.getAllErrors(),HttpStatus.BAD_REQUEST);
            else
            {
                user.setPassword(user.getPassword());
                userService.Saveuser(user,bindingResult);
                return new ResponseEntity<>("user saved", HttpStatus.OK);
            }
        } else
            return new ResponseEntity<>("user already exist with given phone number or email,try login ", HttpStatus.BAD_REQUEST);
    }


}



