package app.ServiceImpl;

import app.model.OTPsystem;
import app.services.PhoneOTP;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.Map;

@Service
public class PhoneOTPimpl implements PhoneOTP {
    private static String ACCOUNT_SID="AC2c6e5f7862e4e8a2c477b5b5c48c31af";
    private static String AUTH_ID="c1106a910d00c0530136dd7e7e32aad4";

    Map<String,OTPsystem> OTPMAP=new HashMap<>();

    static {
        Twilio.init(ACCOUNT_SID,AUTH_ID);
    }

    public ResponseEntity SendOTP(String mobilenumber) {
        OTPsystem OTP = new OTPsystem().GenerateOTP();
        Message.creator(new PhoneNumber(mobilenumber), new PhoneNumber("+15204472733"), "your one time password is" + OTP.getOtp()).create();
        OTPMAP.put(mobilenumber,OTP);
        return new ResponseEntity<>("otp successfully send to :" + mobilenumber, HttpStatus.OK);

    }

    public ResponseEntity<Object> VerifyOTP(String mobilenumber,String otp){

        if(otp.length()!=6)
            return new ResponseEntity<>("please enter a valid otp:",HttpStatus.BAD_REQUEST);

        if(OTPMAP.containsKey(mobilenumber)) {
            OTPsystem OTPsystem = OTPMAP.get(mobilenumber);
            if (OTPsystem.getExpriy() >= System.currentTimeMillis()) {
                if (OTPsystem.getOtp().equals(otp)) {
                    OTPMAP.remove(mobilenumber);
                    return new ResponseEntity<>("mobile number verified", HttpStatus.OK);
                } else return new ResponseEntity<>("invalid otp please try again", HttpStatus.BAD_REQUEST);
            } else {
                OTPMAP.remove(mobilenumber);
                return new ResponseEntity<>("otp expired, please try again", HttpStatus.BAD_REQUEST);
            }
        }
        else return new ResponseEntity<>("mobile number not found",HttpStatus.NOT_FOUND);
    }
}
