package app.ServiceImpl;

import app.model.OTPsystem;
import app.services.MailOTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MailOTPimpl implements MailOTP {
    Map<String,OTPsystem> OTPMAP=new HashMap<>();

    private JavaMailSender javaMailSender;
    @Autowired
    public MailOTPimpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public ResponseEntity SendOTP(String email) {
            try{
                SimpleMailMessage message=new SimpleMailMessage();
                OTPsystem otp=new OTPsystem().GenerateOTP();
                OTPMAP.put(email,otp);
                message.setTo(email);
                message.setFrom("biju.aug1@gmail.com");
                message.setSubject("LostAndFound otp");
                message.setText("hi,\n\tYour one time password  is: "+otp.getOtp());
                javaMailSender.send(message);
                return new ResponseEntity<>("otp is send successfully to "+email,HttpStatus.OK);
            }catch(Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>("something went wrong!please try again.",HttpStatus.BAD_GATEWAY);
            }
        }


    @Override
    public ResponseEntity VerifyOTP(String email, String otp) {
            if(otp.length()!=6)
            return new ResponseEntity<>("please enter a valid otp:",HttpStatus.BAD_REQUEST);

            if(OTPMAP.containsKey(email)){
            OTPsystem otpsystem=OTPMAP.get(email);
            if(otpsystem.getExpriy()>=System.currentTimeMillis())
            {
                if(otpsystem.getOtp().equals(otp))
                {
                    OTPMAP.remove(email);
                    return new ResponseEntity<>("email verified",HttpStatus.OK);
                }
                else
                    return new ResponseEntity<>("incorrect  otp,enter a valid otp",HttpStatus.BAD_REQUEST);
            }
            else{
                    OTPMAP.remove(email);
                    return new ResponseEntity<>("otp expired, please try again", HttpStatus.BAD_REQUEST);
                }
            }
            else return new ResponseEntity<>("email not found",HttpStatus.BAD_REQUEST);

    }
}
