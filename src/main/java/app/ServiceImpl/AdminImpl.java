package app.ServiceImpl;

import app.model.Found;
import app.model.Lost;
import app.services.AdminServices;
import app.services.Founditem;
import app.services.Lostitem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
@Service
@Transactional
public class AdminImpl implements AdminServices {


    private final String username = "Admin_name";
    private final String password = "Admin_password";

    @Autowired
    private Founditem founditem;
    @Autowired
    private Lostitem lostitem;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ResponseEntity AdminLogin(String Username, String Password) {
        if (username != null && password != null) {
            if (Username.equals(username) && Password.equals(password))
                return new ResponseEntity<>("login successful", HttpStatus.OK);
            else
                return new ResponseEntity<>("username and passwords do not match", HttpStatus.BAD_REQUEST);
        } else return new ResponseEntity<>("username and passwords cant be null", HttpStatus.BAD_REQUEST);
    }

    @Override
    public List<Lost> GetLostItems() {
        return lostitem.Showlostitems();
    }

    @Override
    public List<Found> GetFoundItems() {
        return founditem.Showfounditems();
    }

    @Override
    public String RemoveLostItem(int id) {
        try {
            Query query = entityManager.createQuery("delete from Lost l where l.Itemid=" + id);
            int k = query.executeUpdate();
            if (k == 0)
                return " no record with id" + id;
            else
                return "record deleted";
        } catch (Exception e) {
            e.printStackTrace();
            return "unable to delete item";
        }
    }

    @Override
    public String RemoveFoundItem(int id) {
        try {
            Query query = entityManager.createQuery("delete from Found F where F.id=" + id);
            int k = query.executeUpdate();
            if (k == 0)
                return "no record with id " + id;
            else
                return "record removed successfully";
        } catch (Exception e) {
            e.printStackTrace();
            return "unable to delete item";
        }
    }

    @Override
    public String EditLostItems(int id, Lost lost, MultipartFile file) throws IOException {
        try {
            Blob blob = null;
            blob = new SerialBlob(file.getBytes());
            lost.setImage(blob);

            Query query = entityManager.createQuery("update Lost  set Image=:image,Description=:description,Itemname=:itemname,Phone=:phone,LostDate=:lostdate,LostPlace=:lostplace " +
                    "where Itemid=:id");
            query.setParameter("image", lost.getImage());
            query.setParameter("description", lost.getDescription());
            query.setParameter("itemname", lost.getItemname());
            query.setParameter("phone", lost.getPhone());
            query.setParameter("lostdate", lost.getLostDate());
            query.setParameter("lostplace", lost.getLostPlace());
            query.setParameter("id", id);
            int k = query.executeUpdate();
            if(k!=0)
            return " record updated";
            else return "no record exists with id : "+id;
        } catch (SQLException e) {
            e.printStackTrace();
            return "record not updated";
        }

    }

    @Override
    public String EditFoundItems(int id, Found found, MultipartFile file) throws IOException {
        try {
            Blob blob = null;
            blob = new SerialBlob(file.getBytes());
            found.setImage(blob);

            Query query = entityManager.createQuery("update Found  set Image=:image,Description=:description,itemname=:itemname,Phone=:phone,FoundDate=:founddate,FoundPlace=:foundplace " +
                    "where id=:id");
            query.setParameter("image", found.getImage());
            query.setParameter("description", found.getDescription());
            query.setParameter("itemname", found.getItemname());
            query.setParameter("phone", found.getPhone());
            query.setParameter("founddate", found.getFoundDate());
            query.setParameter("foundplace", found.getFoundPlace());
            query.setParameter("id", id);
            int k = query.executeUpdate();
            if(k!=0)
                return " record updated";
            else return "no record exists with id : "+id;
        } catch (SQLException e) {
            e.printStackTrace();
            return "record not updated";
        }

    }
}