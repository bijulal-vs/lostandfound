package app.ServiceImpl;

import app.model.Lost;
import app.repository.ImageRepository;
import app.services.Lostitem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

@Service
@Transactional
public class Lostitemimpl implements Lostitem {

    @Autowired
    private ImageRepository imageRepository;
    @PersistenceContext
    private EntityManager entityManager;
    public  Lostitemimpl(ImageRepository imageRepository){
        this.imageRepository=imageRepository;
    }

    public String SaveItem( Lost lost,MultipartFile file) throws SQLException {
        try {
            Blob blob = new SerialBlob(file.getBytes());
            lost.setImage(blob);
            entityManager.persist(lost);
            return "saved";
        } catch (IOException e) {
            e.printStackTrace();
            return "not saved";
        }
    }

        @Override
    public String SaveLostItem(Lost lost, MultipartFile file, BindingResult bindingResult) throws SQLException {

            if(bindingResult.hasErrors())
                return "please provide valid user details /n"+bindingResult.getAllErrors();
            String Type[]=file.getContentType().split("/");
            String type=Type[Type.length-1];
            System.out.println(type);
            if(type.equals("jpeg")||type.equals("jpg")||type.equals("bmp")||type.equals("pdf"))
                return SaveItem(lost,file);
            else
                return "Image file format not valid,";
    }

    @Override
    public List<Lost> Showlostitems() {
        try {
            Query query = entityManager.createQuery("from Lost order by Itemname");
            return (List<Lost>) query.getResultList();

        }catch (Exception e){
            e.printStackTrace();
            return null;
        } }

    @Override
    public List<Lost> Searchitems(String itemname) {

        if (itemname !=null) {
            try {
                Query query = entityManager.createQuery("from Lost where Itemname= :itemname");
                query.setParameter("itemname", itemname);
                return  (List<Lost>)query.getResultList();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }}
        else
            return null;
    }

    @Override
    public String DeleteLostItem(int id, long phone)
    {
        try{
            if(imageRepository.existsById(id)) {
                Query query = entityManager.createQuery("delete from Lost l where l.Itemid=" + id + "and Phone='" + phone + "'");
                int k = query.executeUpdate();
                if (k == 0)
                    return "you cannot delete this record";
                else
                    return "record removed successfully";
            }else return "no record with given id";
        }catch(Exception e){
            e.printStackTrace();
            return "unable to delete item";
        }
    }



}
