package app.ServiceImpl;

import app.model.CustomUserDetails;
import app.model.userclass;
import app.security.CustomAuthProvider;
import app.security.JwttokenGenerator;
import app.services.Login;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional
public class Loginimpl implements Login {
    @Autowired
    UserService userService;
    @Autowired
    JwttokenGenerator  generator;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CustomAuthProvider customAuthProvider;

    String token;
    CustomUserDetails customuser;
    @Override
    public ResponseEntity UserLogin( long phone, String email,String password) {
        userclass user;

        if (!password.isEmpty()) {

            if (phone != 0)
                user = userService.getbyphone(phone);
            else
            user = userService.getbyemail(email);

            if (user != null) {

                if (passwordEncoder.matches(password,user.getPassword())) {
                     customuser = new CustomUserDetails(user);
                    token=generator.Generate(customuser);
                    System.out.println(token);
                    return new ResponseEntity<>("login success! Welcome", HttpStatus.OK);

                }
                else
                    return new ResponseEntity<>("username and password do not match", HttpStatus.BAD_REQUEST);
            } else
                return new ResponseEntity<>(" the user is not registered, please register to continue", HttpStatus.NOT_FOUND);
        }else
            return new ResponseEntity<>(" Password cannot be empty", HttpStatus.BAD_REQUEST);
    }

    @Override
    public Map<String,String > ShowAllUsers(){
        return userService.getallusernames();
    }


}