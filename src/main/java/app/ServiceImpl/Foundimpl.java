package app.ServiceImpl;

import app.model.Found;
import app.model.userclass;
import app.repository.Imagerepositoryfound;
import app.services.Founditem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.util.List;

@Service
@Transactional
public class Foundimpl implements Founditem {

    @Autowired
    private Imagerepositoryfound imageRepository;
    @PersistenceContext
    private EntityManager entityManager;
    public  Foundimpl(Imagerepositoryfound imageRepository){
        this.imageRepository=imageRepository;
    }

         public String SaveItem(Found found, MultipartFile file) {
            try {
                Blob blob = new SerialBlob(file.getBytes());
                found.setImage(blob);
                entityManager.persist(found);
                return "saved";
            } catch (Exception e) {
                e.printStackTrace();
                return "not saved";
            }
        }

        @Override
        public String AddFoundItem(Found found, MultipartFile file, BindingResult bindingResult) throws IOException{

            if(bindingResult.hasErrors())
                return "please provide valid user details /n"+bindingResult.getAllErrors();

            String Type[]=file.getContentType().split("/");
            String type=Type[Type.length-1];
            System.out.println(type);
            if(type.equals("jpeg")||type.equals("jpg")||type.equals("png")||type.equals("pdf"))
                return SaveItem(found, file);
            else
                return "Image file format not valid";
        }

    @Override
    public List<Found> Searchitem(String itemname) {

        if (itemname !=null) {
            try {
                Query query = entityManager.createQuery("from Found where item_name= :itemname");
                 query.setParameter("itemname", itemname);

              return  (List<Found>)query.getResultList();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }}
         else
            return null;
    }

    @Override
    public List<Found> Showfounditems() {
        try {

            Query query = entityManager.createQuery("from Found order by item_name");
            List<Found> foundlist=query.getResultList();
            for(Found found:foundlist)
                System.out.println("\n"+found.toString());
            return foundlist;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }}

        public String GetAddress(long Phone)
        {
            try{
                Query query=entityManager.createQuery("from userclass u where u.phone='" +Phone+"'");
                userclass user=(userclass) query.getSingleResult();
                return "email: "+user.getEmail()+"Address :"+user.getAddress();
            }catch(Exception e){
                e.printStackTrace();
                return "cannot find user";
            }
        }

    @Override
     public String DeleteFoundItem(int id, long phone)
     {
         try{
             if(imageRepository.existsById(id)) {

                 Query query = entityManager.createQuery("delete from Found F where F.id=" + id + "and Phone='" + phone + "'");
                 int k = query.executeUpdate();
                 if (k == 0)
                     return "you cannot delete this record";
                 else
                     return "record removed successfully";
             }else
                 return "no item with given item id";
         }catch(Exception e){
             e.printStackTrace();
             return "unable to delete item";
         }}

     }









