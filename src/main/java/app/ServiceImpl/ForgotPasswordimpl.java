package app.ServiceImpl;

import app.services.ForgotPassword;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ForgotPasswordimpl implements ForgotPassword  {
    @Autowired
    private MailOTPimpl mailOTPimpl;
    @Autowired
    private UserService userService;

    public ResponseEntity GetOTP(String email){
        if(userService.getbyemail(email)!=null)
            return mailOTPimpl.SendOTP(email);
        else
            return new ResponseEntity<>("email is not yet registered,please register to continue",HttpStatus.BAD_REQUEST);

    }

    public  ResponseEntity ChangePassword(String email,String OTP,String newpassword){
        if(mailOTPimpl.VerifyOTP(email,OTP).getStatusCode().is2xxSuccessful()   ){
            userService.ChangePassword(email,newpassword);
            return new ResponseEntity<>("Password changed Successfully",HttpStatus.OK);
        }else
            return mailOTPimpl.VerifyOTP(email,OTP);


    }

}
